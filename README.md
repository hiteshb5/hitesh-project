
Creating a project for demo purpose.

node {
   def mvnHome
   def scannerHome
   stage('Preparation') {
      git 'https://gitlab.com/marri_joshuadaniel/joshuaproject.git”           
      mvnHome = tool 'MAVEN_HOME'
      scannerHome = tool 'SonarScanner'
   }
   stage('CompileandPackage') {
      // Run the maven build
      withEnv(["MVN_HOME=$mvnHome"]) {
         if (isUnix()) {
            sh '"$MVN_HOME/bin/mvn" -Dmaven.test.failure.ignore clean package'
         } else {
            bat(/"%MVN_HOME%\bin\mvn" -Dmaven.test.failure.ignore clean package/)
         }
      }
   }
   stage('CodeAnalysis') {        
      withSonarQubeEnv("SonarCloud") {
                     sh "${tool("SonarScanner")}/bin/sonar-scanner"
                  }
            }
   stage('DeploytoTomcat') {
      sh 'cp $(pwd)/target/*.war /opt/tomcat/webapps/'
   } 
   stage('FunctionalTesting') {
      sleep 60
      sh label: '', script: 'mvn -Dfilename=testng-functional.xml surefire:test'
   } 
} 
